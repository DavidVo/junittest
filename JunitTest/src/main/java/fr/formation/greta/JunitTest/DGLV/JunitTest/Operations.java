package fr.formation.greta.JunitTest.DGLV.JunitTest;

import java.util.Arrays;

public class Operations {
	
	/**
    public static long additionner(final long...pNombres) {
        long lRetour = 0;
        for(final long lNombre : pNombres) {
        	long x = 1;
        	while (x <= lNombre) {
        		x++;
        		lRetour++;
        	}
        }
        return lRetour;
    }

    public static long multiplier(final long...pNombres) {
        long lRetour = 0;
    	long lres = 1;
        for(final long lNombre : pNombres) {
        	
        	long x = 1;
        	lRetour = 0;
        	while (x <= lNombre) {
        		lRetour +=  lres;
        		x++;
        	}
        	lres = lRetour;
     	
        }
        return lRetour;
    }

    **/

    
	
    public static long diviser(final long...pNombres) {
        if(pNombres.length < 2) {
            throw new IllegalArgumentException(
                    "Il faut au moins deux nombres en entrée");
        }
        long lRetour = pNombres[0];
        for(int i=1;i<pNombres.length;i++) {
            lRetour /= pNombres[i];
        }
        return lRetour;
    }
    

    public static long additionner(long pNombre1, long pNombre2) {
    	
    	if (pNombre2 == 0) return pNombre1;
    	return additionner(pNombre1+1, pNombre2-1);
    }

    public static long additionner(final long...pNombres) {
        long lRetour = 0;
        for(int eachNb = 0; eachNb < pNombres.length; eachNb++) {
        	lRetour = additionner(lRetour, pNombres[eachNb]);
        }
        return lRetour;
    }
    
    public static long multiplier(long pNombre1, long pNombre2) {  	
    	long lRetour = 0;
    	long x = 1;
    	while (x <= pNombre2) {
    		lRetour = additionner(lRetour, pNombre1);
    		x++;
    	}
    	return lRetour;
    }
    

    public static long multiplier(final long...pNombres) {
        long lRetour = 1;
        for(int eachNb = 0; eachNb < pNombres.length; eachNb++) {
        	lRetour = multiplier(lRetour, pNombres[eachNb]);
        }
        return lRetour;
    }
    
	// Cette méthode vérifie que les longueurs passées en paramètre
    // sont celles d'un triangle rectangle
    
    public static Boolean[] pythagore(final long[]...pLongueurs) {
        final Boolean[] lRetours = new Boolean[pLongueurs.length];

        for(int i=0;i<pLongueurs.length;i++) {
            final long[] lLongeurs = pLongueurs[i];
            if(lLongeurs.length != 3) {
                throw new IllegalArgumentException(
                    "Les blocs de longueurs doivent être de 3 éléments");
            }
            
            final long[] lCopieLongueurs = lLongeurs.clone();
            Arrays.sort(lCopieLongueurs);
            
            final long lLongueur1 = lCopieLongueurs[0] * lCopieLongueurs[0];
            final long lLongueur2 = lCopieLongueurs[1] * lCopieLongueurs[1];
            final long lLongueur3 = lCopieLongueurs[2] * lCopieLongueurs[2];
            
            if(lLongueur1 + lLongueur2 == lLongueur3) {
                lRetours[i] = true;
            }
            else {
                lRetours[i] = false;
            }
        }
        return lRetours;
    }

    
}
