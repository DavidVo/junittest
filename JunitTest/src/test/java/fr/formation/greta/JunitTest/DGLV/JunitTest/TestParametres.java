package fr.formation.greta.JunitTest.DGLV.JunitTest;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class TestParametres {
	
	private long mUnResultatAttendu;
	private long[] mNombres;
	
	
	@Parameters
	public static List<Object[]> getParametres(){
		
		return Arrays.asList(
				new Object[] {200L, new long[] {10L,20L} },
				new Object[] {300L, new long[] {10L,30L} },
				new Object[] {400L, new long[] {10L,40L} }
				);
	}
	
	public TestParametres(long pUnResultatAttendu, long[] pNombres) {
		mUnResultatAttendu = pUnResultatAttendu;
		mNombres = pNombres;
	}
	
	@Test
	public void mutiplicationAvecDeuxNombres() {
		assertEquals(mUnResultatAttendu, mutiplication());
	}
	
	
	public long  mutiplication() {
		return mNombres[0]*mNombres[1];
	}

}
