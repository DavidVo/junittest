package fr.formation.greta.JunitTest.DGLV.JunitTest;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class OperationThatTest {
	
	public static long tpsDebut;
	public static long tpsFin;
	public static long tpsTotMethod = 0;
	public static long tpsDebutClass;
	public static long tpsFinClass;
	
	
	
	@Before
	public void tpsBefore() {
		tpsDebut = System.nanoTime();	
	}
	
	@After
	public void tpsAfter() {
		tpsFin = System.nanoTime();
		System.out.println("le temps de la methode est " + (tpsFin-tpsDebut));
		tpsTotMethod += (tpsFin-tpsDebut);
	}
	
	@BeforeClass
	public static void tpsBeforeClass() {
		tpsDebutClass = System.nanoTime();	
	}
	
	@AfterClass
	public static void tpsAfterClass() {
		tpsFinClass = System.nanoTime();
		System.out.println("le temps de la CLASS est " + (tpsFinClass-tpsDebutClass));
		System.out.println("le temps tot methoes est " + tpsTotMethod);
		System.out.println("le temps de calcul est " + ((tpsFinClass-tpsDebutClass)-tpsTotMethod));
	}
	
	
	
	@Test
	public void additionAvecDeuxNombres() {
		assertThat(Operations.additionner(4,2), is(6L));
	}
	
	
	@Test
	public void additionAvecCinqNombres() {
		assertThat(Operations.additionner(1,2,3,4,5), is(15L));
	}
	
	@Test
	public void additionAvecTroisNombres() {
		assertThat(Operations.additionner(1,2,3), is(6L));
	}
	
	
	
	@Test
	public void mutiplicationAvecDeuxNombres() {
		assertThat(Operations.multiplier(2,3),is(6L));
	}
	
	@Test
	public void mutiplicationAvecTroisNombres() {
		assertThat( Operations.multiplier(2,4,6), is(48L));
	}
	
	/**
	@Test(timeout=1)
	public void testMutiplicationAvecCinqNombres() {
		assertThat(Operations.multiplier(256, 512, 1024, 2048, 4096), is(1125899906842624L));
	}
**/
	
	
	@Test(expected=IllegalArgumentException.class)
	public void divisionAvecUnNombre() {
		Operations.diviser(2);
	}
	
	@Test(expected=ArithmeticException.class)
    public void divisionAvecDeuxNombresDontUnZero() {
		assertEquals(ArithmeticException.class,Operations.diviser(10,0));
    }
	
	@Test
	public void tableauAvecValeursDifferentes() {
	final Boolean[] lResutat = Operations.pythagore(
	new long[]{3, 4, 6},
	new long[]{6, 11, 8}
	);
	Assert.assertThat(lResutat, is(new Boolean[]{false, false}));
	}

	
}

