package fr.formation.greta.JunitTest.DGLV.JunitTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OperationsTest {
	
	public static long tpsDebut;
	public static long tpsFin;
	public static long tpsTotMethod = 0;
	public static long tpsDebutClass;
	public static long tpsFinClass;
	
	
	
	
	@Before
	public void tpsBefore() {
		tpsDebut = System.nanoTime();	
	}
	
	@After
	public void tpsAfter() {
		tpsFin = System.nanoTime();
		System.out.println("le temps de la methode est " + (tpsFin-tpsDebut));
		tpsTotMethod += (tpsFin-tpsDebut);
	}
	
	@BeforeClass
	public static void tpsBeforeClass() {
		tpsDebutClass = System.nanoTime();	
	}
	
	@AfterClass
	public static void tpsAfterClass() {
		tpsFinClass = System.nanoTime();
		System.out.println("le temps de la CLASS est " + (tpsFinClass-tpsDebutClass));
		System.out.println("le temps tot methoes est " + tpsTotMethod);
		System.out.println("le temps de calcul est " + ((tpsFinClass-tpsDebutClass)-tpsTotMethod));
	}
	
	
	/**
	
	@Test
	public void additionAvecDeuxNombres() {
		assertEquals(6, Operations.additionner(4,2));
	}
	**/
	
	@Test
	public void additionAvecCinqNombres() {
		assertEquals(15, Operations.additionner(1,2,3,4,5));
	}
	
	@Test
	public void additionAvecTroisNombres() {
		assertEquals(6, Operations.additionner(1,2,3));
	}
	
	
	
	@Test
	public void mutiplicationAvecDeuxNombres() {
		assertEquals(6, Operations.multiplier(2,3));
	}
	
	@Test
	public void mutiplicationAvecTroisNombres() {
		assertEquals(48, Operations.multiplier(2,4,6));
	}
	
	/**
	@Test(timeout=1)
	public void mutiplicationAvecCinqNombres() {
		assertEquals(1125899906842624L, Operations.multiplier(256, 512, 1024, 2048, 4096));
	}
**/
	
	
	@Test(expected=IllegalArgumentException.class)
	public void bdivisionAvecUnNombre() {
		assertEquals(IllegalArgumentException.class,Operations.diviser(2));
	}
	
	@Test(expected=ArithmeticException.class)
    public void bdivisionAvecDeuxNombresDontUnZero() {
		assertEquals(ArithmeticException.class,Operations.diviser(10,0));
    }

	
}
