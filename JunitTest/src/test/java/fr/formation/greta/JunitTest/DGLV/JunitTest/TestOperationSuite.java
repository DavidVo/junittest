package fr.formation.greta.JunitTest.DGLV.JunitTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({OperationsTest.class, TableauTest.class, TestParametres.class})
public class TestOperationSuite {

}
